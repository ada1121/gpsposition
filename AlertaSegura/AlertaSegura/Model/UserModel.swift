//
//  UserModel.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel:NSObject {
    
    var  id:Int?
    var  name: String?
    var  doctype: String?
    var  docnumber: String?
    var  birthdate: String?
    var  email: String?
    var  email_verified_at :String?
    var  phone_code: String?
    var  phone: String?
    var  home_street: String?
    var  home_street_number: String?
    var  home_info_extra: String?
    var  homecity: String?
    var  profilepicture: String?
    var  medical_info: String?
    var  blood_info: String?
    var  emergency_contact_phone: String?
    var  emergency_contact_name: String?
    var  emergency_relation_ship : String?
    var  statuss : Int?
    var  created_at : String?
    var  updated_at : String?
    var  api_key: String?
    var  password: String?
    
    override init() {
        super.init()
           self.id = 0
           self.name = ""
           self.doctype = ""
           self.docnumber = ""
           self.birthdate = ""
           self.email = ""
           self.email_verified_at = ""
           self.phone_code = ""
           self.phone = ""
           self.home_street = ""
           self.home_street_number = ""
           self.home_info_extra = ""
           self.homecity = ""
           self.profilepicture = ""
           self.medical_info = ""
           self.blood_info = ""
           self.emergency_contact_phone = ""
           self.emergency_contact_name = ""
           self.emergency_relation_ship  = ""
           self.statuss = 0
           self.created_at  = ""
           self.updated_at  = ""
           self.api_key = ""
           self.password = ""
    }
    
    init(_ json : JSON) {
        
        self.id = json[PARAMS.ID].intValue
        self.name = json[PARAMS.NAME].stringValue
        self.doctype = json[PARAMS.DOC_TYPE].stringValue
        self.docnumber = json[PARAMS.DOC_NUMBER].stringValue
        self.birthdate = json[PARAMS.BIRTHDAY].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.email_verified_at = json[PARAMS.EMAIL_VERIFIED_AT].stringValue
        self.phone_code = json[PARAMS.PHONECODE].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.home_street = json[PARAMS.HOME_STREET].stringValue
        self.home_street_number = json[PARAMS.HOMESTREET_NUMBER].stringValue
        self.home_info_extra = json[PARAMS.HOME_INFO_EXTRAS].stringValue
        self.homecity = json[PARAMS.HOME_CITY].stringValue
        self.profilepicture = json[PARAMS.PROFIEL_PICTURE].stringValue
        self.medical_info = json[PARAMS.MEDICAL_INFO].stringValue
        self.blood_info = json[PARAMS.BLOOD_INFO].stringValue
        self.emergency_contact_phone = json[PARAMS.EMERGENCY_CONTACT_PHONE].stringValue
        self.emergency_contact_name = json[PARAMS.EMERGENCY_CONTACT_NAME].stringValue
        self.emergency_relation_ship  = json[PARAMS.EMERGENCY_CONTACT_RELATIONSHIP].stringValue
        self.statuss = json[PARAMS.STATUS].intValue
        self.created_at  = json[PARAMS.CREATED_AT].stringValue
        self.updated_at  = json[PARAMS.UPDATED_AT].stringValue
        self.api_key = json[PARAMS.APIKEY].stringValue
        self.password = json[PARAMS.PASSWORD].stringValue
    }
    var isValid: Bool {
        return id != nil && id != 0
    }
    
    func loadUserInfo() {
        
        id = UserDefault.getInt(key: PARAMS.ID, defaultValue: 0)
        name = UserDefault.getString(key: PARAMS.NAME, defaultValue: "")
        doctype = UserDefault.getString(key: PARAMS.DOC_TYPE, defaultValue: "")
        docnumber = UserDefault.getString(key: PARAMS.DOC_NUMBER, defaultValue: "")
        birthdate = UserDefault.getString(key: PARAMS.BIRTHDAY, defaultValue: "")
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        email_verified_at = UserDefault.getString(key: PARAMS.EMAIL_VERIFIED_AT, defaultValue: "")
        phone_code = UserDefault.getString(key: PARAMS.PHONECODE, defaultValue: "")
        phone = UserDefault.getString(key: PARAMS.PHONE, defaultValue: "")
        home_street = UserDefault.getString(key: PARAMS.HOME_STREET, defaultValue: "")
        home_street_number = UserDefault.getString(key: PARAMS.HOMESTREET_NUMBER, defaultValue: "")
        home_info_extra = UserDefault.getString(key: PARAMS.HOME_INFO_EXTRAS, defaultValue: "")
        homecity = UserDefault.getString(key: PARAMS.HOME_CITY, defaultValue: "")
        profilepicture = UserDefault.getString(key: PARAMS.PROFIEL_PICTURE, defaultValue: "")
        medical_info = UserDefault.getString(key: PARAMS.MEDICAL_INFO, defaultValue: "")
        blood_info = UserDefault.getString(key: PARAMS.BLOOD_INFO, defaultValue: "")
        emergency_contact_phone = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_PHONE, defaultValue: "")
        emergency_contact_name = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_NAME, defaultValue: "")
        emergency_relation_ship  = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_RELATIONSHIP, defaultValue: "")
        statuss = UserDefault.getInt(key: PARAMS.STATUS, defaultValue: 0)
        created_at = UserDefault.getString(key: PARAMS.CREATED_AT, defaultValue: "")
        updated_at = UserDefault.getString(key: PARAMS.UPDATED_AT, defaultValue: "")
        api_key = UserDefault.getString(key: PARAMS.APIKEY, defaultValue: "")
        password = UserDefault.getString(key: PARAMS.PASSWORD, defaultValue: "")
    }
    
    func saveUserInfo() {
        UserDefault.setInt(key: PARAMS.ID, value: id!)
        UserDefault.setString(key: PARAMS.NAME, value: name)
        UserDefault.setString(key: PARAMS.DOC_TYPE, value: doctype)
        UserDefault.setString(key: PARAMS.DOC_NUMBER, value: docnumber)
        UserDefault.setString(key: PARAMS.BIRTHDAY, value: birthdate)
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.EMAIL_VERIFIED_AT, value: email_verified_at)
        UserDefault.setString(key: PARAMS.PHONECODE, value: phone_code)
        UserDefault.setString(key: PARAMS.PHONE, value: phone)
        UserDefault.setString(key: PARAMS.HOME_STREET, value: home_street)
        UserDefault.setString(key: PARAMS.HOMESTREET_NUMBER, value: home_street_number)
        UserDefault.setString(key: PARAMS.HOME_INFO_EXTRAS, value: home_info_extra)
        UserDefault.setString(key: PARAMS.HOME_CITY, value: homecity)
        UserDefault.setString(key: PARAMS.PROFIEL_PICTURE, value: profilepicture)
        UserDefault.setString(key: PARAMS.MEDICAL_INFO, value: medical_info)
        UserDefault.setString(key: PARAMS.BLOOD_INFO, value: blood_info)
        UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_PHONE, value: emergency_contact_phone)
        UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_NAME, value: emergency_contact_name)
        UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_RELATIONSHIP, value: emergency_relation_ship)
        UserDefault.setInt(key: PARAMS.STATUS, value: statuss!)
        UserDefault.setString(key: PARAMS.CREATED_AT, value: created_at)
        UserDefault.setString(key: PARAMS.UPDATED_AT, value: updated_at)
        UserDefault.setString(key: PARAMS.APIKEY, value: api_key)
        UserDefault.setString(key: PARAMS.PASSWORD, value: password)
    }
    
    func clearUserInfo() {
        
        self.id = 0
        self.name = ""
        self.doctype = ""
        self.docnumber = ""
        self.birthdate = ""
        self.email = ""
        self.email_verified_at = ""
        self.phone_code = ""
        self.phone = ""
        self.home_street = ""
        self.home_street_number = ""
        self.home_info_extra = ""
        self.homecity = ""
        self.profilepicture = ""
        self.medical_info = ""
        self.blood_info = ""
        self.emergency_contact_phone = ""
        self.emergency_contact_name = ""
        self.emergency_relation_ship  = ""
        self.statuss = 0
        self.created_at  = ""
        self.updated_at  = ""
        self.api_key = ""
        self.password = ""
        
       UserDefault.setInt(key: PARAMS.ID, value: 0)
       UserDefault.setString(key: PARAMS.NAME, value: nil)
       UserDefault.setString(key: PARAMS.DOC_TYPE, value: nil)
       UserDefault.setString(key: PARAMS.DOC_NUMBER, value: nil)
       UserDefault.setString(key: PARAMS.BIRTHDAY, value: nil)
       UserDefault.setString(key: PARAMS.EMAIL, value: nil)
       UserDefault.setString(key: PARAMS.EMAIL_VERIFIED_AT, value: nil)
       UserDefault.setString(key: PARAMS.PHONECODE, value: nil)
       UserDefault.setString(key: PARAMS.PHONE, value: nil)
       UserDefault.setString(key: PARAMS.HOME_STREET, value: nil)
       UserDefault.setString(key: PARAMS.HOMESTREET_NUMBER, value: nil)
       UserDefault.setString(key: PARAMS.HOME_INFO_EXTRAS, value: nil)
       UserDefault.setString(key: PARAMS.HOME_CITY, value: nil)
       UserDefault.setString(key: PARAMS.PROFIEL_PICTURE, value: nil)
       UserDefault.setString(key: PARAMS.MEDICAL_INFO, value: nil)
       UserDefault.setString(key: PARAMS.BLOOD_INFO, value: nil)
       UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_PHONE, value: nil)
       UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_NAME, value: nil)
       UserDefault.setString(key: PARAMS.EMERGENCY_CONTACT_RELATIONSHIP, value: nil)
       UserDefault.setInt(key: PARAMS.STATUS, value: 0)
       UserDefault.setString(key: PARAMS.CREATED_AT, value: nil)
       UserDefault.setString(key: PARAMS.UPDATED_AT, value: nil)
       UserDefault.setString(key: PARAMS.APIKEY, value: nil)
       UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
    }
}
