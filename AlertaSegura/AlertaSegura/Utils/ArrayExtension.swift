//
//  ArrayExtension.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/20/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

extension Array where Element: Equatable {
    func removing(obj: Element) -> [Element] {
        return filter { $0 != obj }
    }
}
