////
////  DBManager.swift
////  CouponExpress
////
////  Created by Elite on 04/12/16.
////  Copyright © 2016 EliteDev. All rights reserved.
////
//
//import Foundation
//import Realm
//import RealmSwift
//let realm = try! Realm()
///// DB Manager
//class DBHelper {
//    // Get the default Realm
//    static let shared = DBHelper()
//
//    private init() {}
//
//    // MARK: - save foodHistorydata
////    func saveHistory(_ savedHistory: FoodHistoryModel) {
////        try! realm.write {
////            realm.create(FoodHistoryModel.self, value: savedHistory, update: .all)
////            realm.refresh()
////        }
////    }
//
//    func updateHistoy(_ id : String, updateModel:FoodHistoryModel) {
//        try! realm.write {
//            let meal = realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)
//
//            if meal != nil{
//                    meal?.categories.removeAll()
//                    for one in updateModel.categories{
//                        meal?.categories.append(one)
//                    realm.add(meal!, update: .modified)
//                    realm.refresh()
//                }
//            }
//            else{
//               realm.create(FoodHistoryModel.self, value: updateModel, update: .all)
//               realm.refresh()
//            }
//        }
//    }
//
//    func getMealHistory( _ id: String) -> FoodHistoryModel {
//        var mealhistory = FoodHistoryModel()
//        if realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id) != nil{
//            mealhistory = realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)!
//        }
//        else{
//            print("No object yet")
//        }
//        return mealhistory
//    }
//
//    func getTotalHistoy() -> Results<FoodHistoryModel> {
//        let totalHistory = realm.objects(FoodHistoryModel.self)
//        return totalHistory
//    }
//
//    func removewithIndex(_ id: String) {
//       do {
//           //let objects = realm.objects(FoodHistoryModel.self)
//           try! realm.write {
//            realm.delete(realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)!)
//           }
//       } catch let error as NSError {
//           // handle error
//           print("error - \(error.localizedDescription)")
//       }
//    }
//
//    func removeall(){
//         try! realm.write {
//            realm.deleteAll()
//            realm.refresh()
//         }
//    }
//}
