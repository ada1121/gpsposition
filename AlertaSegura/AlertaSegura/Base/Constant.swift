
import Foundation
import UIKit
import SwiftyUserDefaults

//*****************************************************************//
                        //Global Variable//
//*****************************************************************//

var loginNav : UINavigationController!
var mainNav : UINavigationController!
let durationOfAnimationInSecond = 1.0
var alertController : UIAlertController? = nil


let timeNow = Int(NSDate().timeIntervalSince1970)
var normal : Bool = true
var birthdate : String? = nil
var selectedLocation : String? = nil
var selectedDoctype : String? = nil

struct Constants {
    static let SAVE_ROOT_PATH = "psj"
}

