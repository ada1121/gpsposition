//
//  BaseVC1.swift
//  AletaSequra
//
//  Created by PSJ on 01/30/20.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON
import KRProgressHUD
import KRActivityIndicatorView

class BaseVC1 : UIViewController {
    
    var gradientLayer: CAGradientLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func createGradientView(_ targetView : UIView) {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
    }
    
    func createGradientLabel(_ targetView : UIView, letter : String,fontsize : Int ,position : Int) {
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Create a label and add it as a subview
        let label = UILabel(frame: targetView.bounds)
        label.text = letter
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
        
        if position == 0 {
            label.textAlignment = .center
        }
        else if position == 1{
            label.textAlignment = .left
        }
        
        else if position == 2{
            label.textAlignment = .right
        }
        
        targetView.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        targetView.mask = label
    }
    
    func transitionAnimation(view: UIView, animationOptions: UIView.AnimationOptions, isReset: Bool) {
      UIView.transition(with: view, duration: durationOfAnimationInSecond, options: animationOptions, animations: {
        view.backgroundColor = UIColor.init(named: isReset ? "darkGreen" :  "darkRed")
      }, completion: nil)
    }
    
//    func showMessage1(_ message : String) {
//        self.view.makeToast(message)
//    }
    
    func setProgressHUDStyle(_ style: Int,backcolor: UIColor,textcolor : UIColor, imagecolor : UIColor ) {
        
            if style != 2{
            let styles: [KRProgressHUDStyle] = [.white, .black, .custom(background: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), text: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1), icon: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))]
                KRProgressHUD.set(style: styles[style]) }
            
            else{
                let styles : KRProgressHUDStyle = .custom (background:backcolor,text : textcolor, icon: imagecolor )
                 KRProgressHUD.set(style: styles)
                
            }
    }
    
    func showProgressSet_withMessage(_ msg : String, msgOn : Bool,styleVal: Int ,backColor: UIColor,textColor : UIColor,imgColor : UIColor , headerColor : UIColor, trailColor : UIColor)  {
    
    setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgColor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        KRProgressHUD.show(withMessage: msgOn == false ? nil : msg)
    }
    
    func progressSet(styleVal: Int ,backColor: UIColor,textColor : UIColor , imgcolor: UIColor, headerColor : UIColor, trailColor : UIColor)  {
    
        setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgcolor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        
    }

    func progShowSuccess(_ msgOn:Bool, msg:String){
        KRProgressHUD.showSuccess(withMessage: msgOn == false ? nil : msg)
    }

    func progShowError(_ msgOn:Bool, msg:String) {
        KRProgressHUD.showError(withMessage: msgOn == false ? nil : msg)
    }

    func progShowWithImage(_ msgOn:Bool, msg:String ,image : String) {
        let image = UIImage(named:image)!
        KRProgressHUD.showImage(image, message: msgOn == false ? nil : msg)
    }
    
    func progShowInfo(_ msgOn:Bool, msg:String) {
        KRProgressHUD.showInfo(withMessage: msgOn == false ? nil : msg)
    }
    
    func showProgress() {
        KRProgressHUD.show()
    }
    
    func hideProgress() {
        KRProgressHUD.dismiss()
    }
    
    func resetProgress() {
        KRProgressHUD.resetStyles()
    }
    
    func alertMake(_ msg : String) -> UIAlertController {
        alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController!
    }
    
    func alertDisplay(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func gotoVC(_ nameVC: String){
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
        print("herereached")
    }
    
    func gotoNavPresent1(_ storyname : String) {
              
          let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
          toVC?.modalPresentationStyle = .fullScreen
          self.navigationController?.pushViewController(toVC!, animated: false)
            
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func goSetting() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
   }
    
    func navBarTransparent1() {
      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() {
         self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
}




