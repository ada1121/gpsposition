//
//  R.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import Foundation

struct R {
    struct string {
        static let app_name = "Villarino Seguro"
        static let enteremail = "Please enter email address"
        static let entervalidemail = "Error: email inválido address"
        static let enterpassword = "Introducir contraseña "
        static let wentwrong = "Algo salió mal"
        static let turnonlocationgps = "Encender Localización"
        static let alertsent = "Alerta Enviada"
        static let selectlocaliad = "Seleccione Localidad item"
        static let selecttopo = "Seleccione Tipo de documento"

        static let enterfullname = "Por favor cargue Nombre Completo"
        static let enterfirstname = "Por favor cargue Nombre"
        static let selectdoctype = "Seleccionar tipo de documento"
        static let enterdocnumber = "Por favor introducir numero de documento"
        static let enterstreet = "Introducir Nombre de Calle"
        static let enterstreetnumber = "Introducir Numero"
        static let selectacity = "Seleccionar Ciudad"
        static let entercompletedphonenumber = "Introducir Telefono completo"
        static let enterconfirmpassword = "Confirmar Contraseña"
        static let entercorrectconfirmpassword = "Inroducir Contraseña correcta"

        static let cancel = "Cancel"

        static let emailaddress = "email address"
        static let password = "contraseña"
        static let signin = "Log in"
        static let updateprofile = "Actualizar Perfil"
        static let signup = "Registrarse"
        static let sendingalert = "Enviando Alerta"
        static let termsandcondition = "Términos y Condiciones"
        static let firstterms = "Este es un SERVICIO COMPLEMENTARIO al 911"
        static let secondterms = "Solo funciona DENTRO del Municipio de VILLARINO y para quienes viven aquí"
        static let thridterms = "Esta App solo funciona con DATOS y GEOLOCALIZACION activada"
        static let accept = " Si, Acepto"
    }
}
