
import UIKit
import EventKit


class CalnedarView: BaseVC1 {

    
    @IBOutlet weak var calendarViewV: CalendarView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var selectedDate : Date? =  nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let style = CalendarView.Style()
        
        
        style.cellShape                = .round
        style.cellColorDefault         = UIColor.clear
        style.cellColorToday           = UIColor.systemRed
        style.cellSelectedBorderColor  = UIColor.red
        style.cellEventColor           = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.headerTextColor          = UIColor.darkGray

        style.cellTextColorDefault     = UIColor.black
        style.cellTextColorToday       = UIColor.white
        style.cellTextColorWeekend     = UIColor.systemPink
        style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)

        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday

        style.locale                   = NSLocale.current

        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 24.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        
        calendarViewV.style = style
        
        calendarViewV.dataSource = self
        calendarViewV.delegate = self
        
        calendarViewV.direction = .horizontal
        calendarViewV.multipleSelectionEnable = false
        calendarViewV.marksWeekends = true
        
        
        calendarViewV.backgroundColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
    }
    
    
    @IBAction func selectBtnClicked(_ sender: Any) {
        if selectedDate == nil{
            self.alertDisplay(alertController: self.alertMake("Please select birthday"))
        }
        else{
            let dateString : String = "\(selectedDate!)"
            let components = dateString.split(withMaxLength: 10)
            birthdate = String(components[0])
            print(birthdate)
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        let today = Date()
        
        var tomorrowComponents = DateComponents()
        tomorrowComponents.day = 0 // selected date setting
       

        #if KDCALENDAR_EVENT_MANAGER_ENABLED
        self.calendarViewV.loadEvents() { error in
            if error != nil {
                let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        #endif
        
        
        self.calendarViewV.setDisplayDate(today)
        
        self.datePicker.locale = self.calendarViewV.style.locale
        self.datePicker.timeZone = self.calendarViewV.calendar.timeZone
        self.datePicker.setDate(today, animated: false)
    
    }
    
    
    // MARK : Events
    
    @IBAction func onValueChange(_ picker : UIDatePicker) {
        self.calendarViewV.setDisplayDate(picker.date, animated: true)
    }
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        self.calendarViewV.goToPreviousMonth()
    }
    @IBAction func goToNextMonth(_ sender: Any) {
        self.calendarViewV.goToNextMonth()
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}


extension CalnedarView: CalendarViewDataSource {
    // MARK:here we can add start limited day
      func startDate() -> Date {
          
          var dateComponents = DateComponents()
          dateComponents.month = -1500
          
          let today = Date()
          
          let threeMonthsAgo = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)!
          
          return threeMonthsAgo
      }
      // MARK: here we can add start limited day
      func endDate() -> Date {
          
          var dateComponents = DateComponents()
        
          dateComponents.month = 1200
          let today = Date()
          
          let twoYearsFromNow = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)!
          
          return twoYearsFromNow
    
      }
    
}

extension CalnedarView: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
           selectedDate = date
           print("Did Select: \(date) with \(events.count) events")
           for event in events {
               print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
           }
           
       }
       
       func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
           print(self.calendarViewV.selectedDates)
           
           self.datePicker.setDate(date, animated: true)
       }
}







