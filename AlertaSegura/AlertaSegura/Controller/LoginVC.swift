//
//  LoginVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import TransitionButton
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyUserDefaults
import KRProgressHUD
import KRActivityIndicatorView

class LoginVC: BaseVC1 ,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var animationBtn: TransitionButton!
    
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    var validation:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadlayout()
    }
    func loadlayout()  {
        self.edtPwd.text = "123456"
        self.edtEmail.text = "pak@gmail.com"
    }
    @IBAction func clickAnimation(_ sender: Any) {
        self.loginBtnClicked()
        animationBtn.startAnimation() // 2: Then start the animation when the user tap the button
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
                DispatchQueue.main.async(execute: { () -> Void in
                  
                    if self.validation{
                        self.loginApi(useremail: self.edtEmail.text!, pwd: self.edtPwd.text!)
                    }
                    else{
                        self.animationBtn.stopAnimation()
                        self.display(alertController: self.alertMake("Input Correct!"))
                    }

                })
            })
    }
    func loginBtnClicked() {
        validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
         validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        
         validator.styleTransformers(success:{ (validationRule) -> Void in
             
             // clear error label
             validationRule.errorLabel?.isHidden = true
             validationRule.errorLabel?.text = ""
             
             if let textField = validationRule.field as? UITextField {
                 textField.layer.borderColor = UIColor.green.cgColor
                 textField.layer.borderWidth = 1
             } else if let textField = validationRule.field as? UITextView {
                 textField.layer.borderColor = UIColor.green.cgColor
                 textField.layer.borderWidth = 1
             }
         }, error:{ (validationError) -> Void in
             print("error")
             validationError.errorLabel?.isHidden = false
             validationError.errorLabel?.text = validationError.errorMessage
             if let textField = validationError.field as? UITextField {
                 textField.layer.borderColor = UIColor.red.cgColor
                 textField.layer.borderWidth = 1.0
             } else if let textField = validationError.field as? UITextView {
                 textField.layer.borderColor = UIColor.red.cgColor
                 textField.layer.borderWidth = 1.0
             }
         })
         validator.validate(self)
    }
    func validationSuccessful() {
       validation = true
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
        validation = false
    }
    
    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    func loginApi(useremail : String, pwd : String) {
        
        ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
            
            if isSuccess{
                if data != nil{
                    self.animationBtn.stopAnimation(animationStyle: .expand, completion: {
                        self.gotoVC("MainNav")
                    })
                }
                else{
                    self.animationBtn.stopAnimation()
                    self.display(alertController: self.alertMake("Las credenciales enviadas no son correctas"))
                }
            }
            else{
                self.animationBtn.stopAnimation()
                self.display(alertController: self.alertMake("Las credenciales enviadas no son correctas"))
            }
        }
    }
}
