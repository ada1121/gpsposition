//
//  DoctypeVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/30/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class DoctypeVC: BaseVC1{
    
    @IBOutlet weak var sampleTable: UITableView!
    
    
    var cellHeight = 50
    let sampleArray =  ["DNI","CI","PAS","LE","LC"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
             sampleTable.roundCorners([.bottomLeft, .bottomRight], radius: 20)
    }

}

extension DoctypeVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        cell.sampleMeal.text = sampleArray[indexPath.row]
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: false)
        selectedDoctype = sampleArray[indexPath.row]
    }
}


