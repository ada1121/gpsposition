//
//  TermsConditionVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class TermsConditionVC: BaseVC1 {

    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var signBtn: UIButton!
    var status = 0
    @IBOutlet weak var termsContent: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        termsContent.text = R.string.firstterms
        signBtn.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    @IBAction func nextBtnClicked(_ sender: Any) {
        
        status += 1
        showStatues()
    }
    func showStatues()  {
        if status == 1{
            termsContent.text = R.string.secondterms
            self.signBtn.isHidden = true
        }
        if status == 2{
            termsContent.text = R.string.thridterms
            nextBtn.isHidden = true
            self.signBtn.isHidden = false
        }
    }
    @IBAction func signBtnClicked(_ sender: Any) {
        if normal { self.navigationController?.popToRootViewController(animated: true)
            
        }
        else{
            self.gotoNavPresent1("SignUpVC")
        }
    }
}
