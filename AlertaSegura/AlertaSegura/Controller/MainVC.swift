//
//  MainVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/27/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import CircleProgressView
import CoreLocation
import SwiftyJSON

class MainVC: BaseVC1, CLLocationManagerDelegate {

    @IBOutlet weak var policeProgress: CircleProgressView!
    @IBOutlet weak var ambluenceProgress: CircleProgressView!
    @IBOutlet weak var fireProgress: CircleProgressView!
    @IBOutlet weak var policeBack: UIView!
    @IBOutlet weak var ambluenceBack: UIView!
    @IBOutlet weak var fireBack: UIView!
    @IBOutlet weak var imvPolice: UIImageView!
    @IBOutlet weak var imvAmbluence: UIImageView!
    @IBOutlet weak var imvFire: UIImageView!
    @IBOutlet weak var imvPoliceCheck: UIImageView!
    @IBOutlet weak var imvAmbluenceCheck: UIImageView!
    @IBOutlet weak var imvFireCheck: UIImageView!
    
    @IBOutlet weak var fireView: UIView!
    @IBOutlet weak var ambluenceView: UIView!
    @IBOutlet weak var policeView: UIView!
    var policeTimerState : Bool = true
    var ambuluenceTimerState : Bool = true
    var fireTimerSate : Bool = true
    
    
    var gameTimer1: Timer?
    var gameTimer2: Timer?
    var gameTimer3: Timer?
    var selectedState = 0
    let locationManager = CLLocationManager()
    
    var timer:Timer! = nil
    var lpr1:UILongPressGestureRecognizer! = nil
    var lpr2:UILongPressGestureRecognizer! = nil
    var lpr3:UILongPressGestureRecognizer! = nil
    
    var lat : Double?
    var lang : Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkInit()
        imageInit()
        backInit()
        
        birthdate = UserDefault.getString(key: PARAMS.BIRTHDAY)!
        selectedLocation  = UserDefault.getString(key: PARAMS.HOME_CITY)!
        selectedDoctype  = UserDefault.getString(key: PARAMS.DOC_TYPE)!
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        lpr1 = UILongPressGestureRecognizer()
        lpr1.minimumPressDuration = 0.5
        lpr1.numberOfTouchesRequired = 1
        lpr1.addTarget(self, action: #selector(doTouchActions))
        
        lpr2 = UILongPressGestureRecognizer()
        lpr2.minimumPressDuration = 0.5
        lpr2.numberOfTouchesRequired = 1
        lpr2.addTarget(self, action: #selector(doTouchActions))
        
        lpr3 = UILongPressGestureRecognizer()
        lpr3.minimumPressDuration = 0.5
        lpr3.numberOfTouchesRequired = 1
        lpr3.addTarget(self, action: #selector(doTouchActions))

        self.policeView.addGestureRecognizer(lpr1)
        self.ambluenceView.addGestureRecognizer(lpr2)
        self.fireView.addGestureRecognizer(lpr3)
    }
    
    /// ======================//
    
    func createTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(cancelTrouchFromTimer), userInfo: nil, repeats: false)
        }
    }

    func cancelTimer() {
        if timer != nil {
        timer.invalidate()
        timer = nil
        }
        self.policeTimerState = false
        self.ambuluenceTimerState = false
        self.fireTimerSate = false
        //self.stopPoliceProgress()
        print("timernil")
    }

    @objc func cancelTrouchFromTimer() {
        
        if selectedState == 1{// automatic timer action
        lpr1.isEnabled = false
        self.policeTimerState = false
        //do all the things
        print("longpressstop1")
        lpr1.isEnabled = true
        }
        else if selectedState == 2{// automatic timer action
        lpr2.isEnabled = false
            self.ambuluenceTimerState = false
        //do all the things
        print("longpressstop2")
        lpr2.isEnabled = true
        }
        else if selectedState == 3{// automatic timer action
        lpr3.isEnabled = false
        self.fireTimerSate = false
        //do all the things
        print("longpressstop3")
        lpr3.isEnabled = true
        }
        else{
            print("No guesture")
        }
    }

    @objc func doTouchActions(_ sender: UILongPressGestureRecognizer) {
        print("sendertag ===\(sender.view!.tag)")
        if sender.view?.tag == 1{
            if sender.state == .began {
                // add action
                self.policeTimerState = true
                self.policeTouchAction()
                createTimer()
                print("startLongPress1")
            }

            if sender.state == .ended {
                self.policeTimerState = false
                cancelTimer()
                print("cancelLongPress1")
            }
        }
        else if sender.view?.tag == 2{
            if sender.state == .began {
                // add action
                self.ambuluenceTimerState = true
                self.ambluenceTouchAction()
                createTimer()
                print("startLongPress2")
            }

            if sender.state == .ended {
                self.ambuluenceTimerState = false
                cancelTimer()
                print("cancelLongPress2")
            }
        }
        else if sender.view?.tag == 3{
            if sender.state == .began {
                // add action
                self.fireTimerSate = true
                self.fireTouchAction()
                createTimer()
                print("startLongPress3")
            }

            if sender.state == .ended {
                self.fireTimerSate = false
                cancelTimer()
                print("cancelLongPress3")
            }
        }
        else{
          print("default")
        }
            
        
    }

    // cancel timer for all cases where the view could go away, like in deInit
    func `deinit`() {
        cancelTimer()
    }
    
    // =========================================================
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        lang = locValue.longitude
        
    }
    
    func imageInit() {
        self.imvPolice.isHidden = false
        self.imvAmbluence.isHidden = false
        self.imvFire.isHidden = false
    }
    func backInit()  {
        self.policeBack.backgroundColor = UIColor.init(named: "colorProgBack")
        self.ambluenceBack.backgroundColor = UIColor.init(named: "colorProgBack")
        self.fireBack.backgroundColor = UIColor.init(named: "colorProgBack")
    }
    func checkInit()  {
        self.imvPoliceCheck.isHidden = true
        self.imvAmbluenceCheck.isHidden = true
        self.imvFireCheck.isHidden = true
    }
    
    func gotoApi(_ type: Int)  {
        
        let client_id = user?.id
        let api_key = user?.api_key

            
        ApiManager.alert(client_id: client_id!, type: type, lat: -34.535445, lang: -58.470471, api_key: api_key!) { (isSuccess, data) in
            
            if isSuccess{
                
                if data as! String != ""{
                    self.display(alertController: self.alertMake("La alerta se ha enviado!"))
                }
                else{
                    self.display(alertController: self.alertMake("Se ha producido un error"))
                }
            }
            else{
                self.display(alertController: self.alertMake("Se ha producido un error"))
            }
        }
    }
    
    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    func policeTouchAction() {
        self.policeProgress.progress = Double(0)
               selectedState = 1
               var state = 0
               Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { timer in
                   
                if self.policeTimerState{
                    let progress : Double = 1/150
                    self.policeProgress.progress += Double(progress)
                   var delay = 0
                
                   self.delay1(3){
                       () in
                       state = 1
                   }

                   if state == 1 {
                       timer.invalidate()
                       do {
                       self.policeProgress.progress = Double(10)
                       self.imvPolice.isHidden = true
                       self.policeBack.backgroundColor = UIColor.init(named: "colorProgBackEnd")
                       self.imvPoliceCheck.isHidden = false
                       delay = 1
                       self.gotoApi(1)
                       }
                       if delay == 1{
                           self.delay1(1){
                               () in
                               
                               self.policeInit()
                           }
                       }
                       else if delay == 0{
                           return
                       }
                   }
                   else{
                       return
                   }
                }
                else{
                    timer.invalidate()
                    self.stopPoliceProgress()
                }
                
        }
    }
    
    func ambluenceTouchAction() {
        self.ambluenceProgress.progress = Double(0)
               selectedState = 2
               var state = 0
               Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { timer in
                   
                if self.ambuluenceTimerState{
                    let progress : Double = 1/150
                    self.ambluenceProgress.progress += Double(progress)
                   var delay = 0
                
                   self.delay1(3){
                       () in
                       state = 1
                   }

                   if state == 1 {
                       timer.invalidate()
                       do {
                       self.ambluenceProgress.progress = Double(10)
                        self.imvAmbluence.isHidden = true
                        self.ambluenceBack.backgroundColor = UIColor.init(named: "colorProgBackEnd")
                        self.imvAmbluenceCheck.isHidden = false
                        delay = 1
                        self.gotoApi(2)
                       }
                       if delay == 1{
                           self.delay1(1){
                               () in
                            self.ambuluenceInit()                           }
                       }
                       else if delay == 0{
                           return
                       }
                   }
                   else{
                       return
                   }
                }
                else{
                    timer.invalidate()
                    self.stopAmbuluenceProgress()
                }
        }
    }
    
    func fireTouchAction() {
        self.fireProgress.progress = Double(0)
               selectedState = 3
               var state = 0
               Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) { timer in
                   
                if self.fireTimerSate{
                    let progress : Double = 1/150
                    self.fireProgress.progress += Double(progress)
                   var delay = 0
                
                   self.delay1(3){
                       () in
                       state = 1
                   }

                   if state == 1 {
                       timer.invalidate()
                       do {
                       self.fireProgress.progress = Double(10)
                        self.imvFire.isHidden = true
                        self.fireBack.backgroundColor = UIColor.init(named: "colorProgBackEnd")
                        self.imvFireCheck.isHidden = false
                        delay = 1
                        self.gotoApi(3)
                       }
                       if delay == 1{
                           self.delay1(1){
                               () in
                            self.fireInit()
                           }
                       }
                       else if delay == 0{
                           return
                       }
                   }
                   else{
                       return
                   }
                }
                else{
                    timer.invalidate()
                    self.stopFireProgress()
                }
        }
    }
    
    func stopPoliceProgress()  {
        self.policeInit()
    }
    func stopAmbuluenceProgress()  {
        self.ambuluenceInit()
    }
    func stopFireProgress()  {
        self.fireInit()
    }
    
    func policeInit()  {
         self.imvPolice.isHidden = false
         self.policeBack.backgroundColor = UIColor.init(named: "colorProgBack")
         self.imvPoliceCheck.isHidden = true
         self.policeProgress.progress = Double(0)
    }
    func ambuluenceInit()  {
         self.imvAmbluence.isHidden = false
         self.ambluenceBack.backgroundColor = UIColor.init(named: "colorProgBack")
         self.imvAmbluenceCheck.isHidden = true
         self.ambluenceProgress.progress = Double(0)
    }
    func fireInit()  {
         self.imvFire.isHidden = false
         self.fireBack.backgroundColor = UIColor.init(named: "colorProgBack")
         self.imvFireCheck.isHidden = true
         self.fireProgress.progress = Double(0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func delay1(_ delay:Double, closure: @escaping ()-> Void) {
        let delayTime = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: closure)
    }

    @IBAction func profileUpdateBtnClicked(_ sender: Any) {
        self.gotoNavPresent1("ProflleUpdateVC")
    }
    
}

