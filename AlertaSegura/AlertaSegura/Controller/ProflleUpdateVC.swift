


//
//  SignUpVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import TransitionButton
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyUserDefaults
import KRProgressHUD
import KRActivityIndicatorView
import SwiftyMenu

class ProflleUpdateVC :BaseVC1,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var nomcreComp: UITextField!// full name
    @IBOutlet weak var apellido: UITextField!// firstname
    @IBOutlet weak var fecha: UITextField! // birthdate
    @IBOutlet weak var tipo: UITextField! // doctype
    @IBOutlet weak var numero: UITextField! // doctypenumber
    @IBOutlet weak var calle: UITextField! // street
    @IBOutlet weak var direction_numero: UITextField! // street nmber
    @IBOutlet weak var datos: UITextField! //street extra
    @IBOutlet weak var localidad: UITextField! // homecity
    @IBOutlet weak var correo: UITextField! //email
    @IBOutlet weak var phone1_1: UITextField! //phonecode1
    @IBOutlet weak var phone1_2: UITextField! //phonenumber1
    @IBOutlet weak var dato: UITextField! // medial
    @IBOutlet weak var grupo: UITextField! // group
    @IBOutlet weak var phone2_1: UITextField!// emerphpnecode1
    @IBOutlet weak var phone2_2: UITextField!//emerphone2
    @IBOutlet weak var nombreDe: UITextField! // emername
    @IBOutlet weak var relacion: UITextField! //emerrealation
    @IBOutlet weak var contrasena: UITextField! // password
    @IBOutlet weak var contrasena_new: UITextField! // password
    @IBOutlet weak var confirmar: UITextField! // confirmpassword
    @IBOutlet weak var animationBtn: TransitionButton!
    
    
    let validator = Validator()
    var validation:Bool = false
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
    }
    
    @IBAction func clickDropDown(_ sender: Any) {
        self.gotoNavPresent1("DropViewVC")
    }
    @IBAction func clickDocType(_ sender: Any) {
        self.gotoNavPresent1("DoctypeVC")
    }
    
    func editInit(){
        
        let fullName = UserDefault.getString(key: PARAMS.NAME)!

        let components = fullName.split{ !$0.isLetter }
        var firstName = ""
        var lastName = ""
        print(components)
        if components.count == 2 {
             firstName = String(components[0])
             lastName = String(components[1])
        }
        else{
            firstName = fullName
            lastName = ""
        }
       
       
        nomcreComp.addPadding(.left(20))
        nomcreComp.text =  firstName
        
        
        
        apellido.addPadding(.left(20))
        apellido.text =  lastName
        
        
        fecha.addPadding(.left(20))
        fecha.text =  UserDefault.getString(key: PARAMS.BIRTHDAY)!
        
        
        tipo.addPadding(.left(20))
        tipo.text =  UserDefault.getString(key: PARAMS.DOC_TYPE)!
        
        
        
        numero.addPadding(.left(20))
        numero.text =  UserDefault.getString(key: PARAMS.DOC_NUMBER)!
        
        
        calle.addPadding(.left(20))
        calle.text =  UserDefault.getString(key: PARAMS.HOME_STREET)!
        
        
        direction_numero.addPadding(.left(20))
        direction_numero.text =  UserDefault.getString(key: PARAMS.HOMESTREET_NUMBER)!
        
        
        
        datos.addPadding(.left(20))
        datos.text = UserDefault.getString(key: PARAMS.HOME_INFO_EXTRAS)!
        
        
        localidad.addPadding(.left(20))
        localidad.text =  UserDefault.getString(key: PARAMS.HOME_CITY)!
    
        
        
        correo.addPadding(.left(20))
        correo.text = UserDefault.getString(key: PARAMS.EMAIL)!
        
        
        
        dato.addPadding(.left(20))
        dato.text =  UserDefault.getString(key: PARAMS.MEDICAL_INFO)!
        
        
        grupo.addPadding(.left(20))
        grupo.text = UserDefault.getString(key: PARAMS.BLOOD_INFO)!
        
        fecha.text =  UserDefault.getString(key: PARAMS.BIRTHDAY)!
        tipo.text = UserDefault.getString(key: PARAMS.DOC_TYPE)!
        
        nombreDe.addPadding(.left(20))
        nombreDe.text = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_NAME)!
        
        contrasena.text = UserDefault.getString(key: PARAMS.PASSWORD)!
        
        relacion.addPadding(.left(20))
        relacion.text =  UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_RELATIONSHIP)!
        
        phone1_1.text = UserDefault.getString(key: PARAMS.PHONECODE)!
        
        phone1_2.text = UserDefault.getString(key: PARAMS.PHONE)!
        
//        phone2_1.text = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_PHONE)!
        
        phone2_2.text = UserDefault.getString(key: PARAMS.EMERGENCY_CONTACT_PHONE)!
        
        
        contrasena.addPadding(.left(20))
        contrasena.attributedPlaceholder = NSAttributedString(string: "Contraseña Actual",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        
        confirmar.addPadding(.left(20))
        confirmar.attributedPlaceholder = NSAttributedString(string: "Confirmar Contraseña NUEVA",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        contrasena_new.addPadding(.left(20))
        contrasena_new.attributedPlaceholder = NSAttributedString(string: "Contraseña NUEVA",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        

    }
    @IBAction func calendarSelected(_ sender: Any) {
        self.gotoNavPresent1("CalnedarView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fecha.text = birthdate
        localidad.text = selectedLocation
        tipo.text = selectedDoctype
    }
    
    @IBAction func clickAnimation(_ sender: Any) {
        self.signupBtnClicked()
              animationBtn.startAnimation() // 2: Then start the animation when the user tap the button
                  let qualityOfServiceClass = DispatchQoS.QoSClass.background
                  let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
                  backgroundQueue.async(execute: {
                      DispatchQueue.main.async(execute: { () -> Void in
                         
                          if self.validation{
                            self.signUpapi()
                          }
                          else{
                              self.animationBtn.stopAnimation()
                              self.display(alertController:self.alertMake("Input Correct!"))
                          }

                      })
                  })
       }
    func signupBtnClicked() {
        
        fecha.text = birthdate
        localidad.text = selectedLocation
        tipo.text = selectedDoctype

        validator.registerField(nomcreComp, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(apellido, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(fecha, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(tipo, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(numero, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(direction_numero, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(calle, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(datos, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(localidad, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(correo, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
        validator.registerField(phone1_1, errorLabel: nil , rules: [RequiredRule()])
        validator.registerField(phone1_2, errorLabel: nil , rules: [RequiredRule()])
        
        
        validator.registerField(contrasena, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
         validator.registerField(contrasena_new, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.registerField(confirmar, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: contrasena_new)])
        
         validator.styleTransformers(success:{ (validationRule) -> Void in
             
             // clear error label
             validationRule.errorLabel?.isHidden = true
             validationRule.errorLabel?.text = ""
             
             if let textField = validationRule.field as? UITextField {
                 textField.layer.borderColor = UIColor.green.cgColor
                 textField.layer.borderWidth = 1
             } else if let textField = validationRule.field as? UITextView {
                 textField.layer.borderColor = UIColor.green.cgColor
                 textField.layer.borderWidth = 1
             }
         }, error:{ (validationError) -> Void in
             print("error")
             validationError.errorLabel?.isHidden = false
             validationError.errorLabel?.text = validationError.errorMessage
             if let textField = validationError.field as? UITextField {
                 textField.layer.borderColor = UIColor.red.cgColor
                 textField.layer.borderWidth = 1.0
             } else if let textField = validationError.field as? UITextView {
                 textField.layer.borderColor = UIColor.red.cgColor
                 textField.layer.borderWidth = 1.0
             }
         })
         validator.validate(self)
    }
    
    func validationSuccessful() {
           validation = true
        }
        
        func validationFailed(_ errors: [(Validatable, ValidationError)]) {
            print("validation error")
            validation = false
        }
        
        func display(alertController: UIAlertController) {
            self.present(alertController, animated: true, completion: nil)
        }
        
        func signUpapi() {
            
            let name : String = apellido.text! + " " + nomcreComp.text!
            let birthday : String = birthdate ?? ""
            
            let homecity : String = selectedLocation!
            let emergencycontactPhone : String = phone2_1.text! + phone2_2.text!
            let homestreetnumber = direction_numero.text!
            //let homestreetnumber = "548"

            ApiManager.update(name: name, doc_type: selectedDoctype!, doc_number: numero.text!, birth_date: birthday, email: correo.text!, password: confirmar.text!, phone_code: phone1_1.text!, phone: phone1_2.text!, home_street: calle.text!, home_street_number: homestreetnumber, home_info_extras: datos.text!, home_city: homecity, medical_info: dato.text!, blood_info: grupo.text!, emergency_contact_phone: emergencycontactPhone, emergency_contact_name: nombreDe.text!, emergency_contact_relationship: relacion.text!) { (isSuccess, data) in
                
                if isSuccess{
                    if user.isValid{
                        self.animationBtn.stopAnimation(animationStyle: .expand, completion: {
                                               self.gotoVC("MainNav")
                                           })
                    }
                    else{
                        self.animationBtn.stopAnimation()
                        self.display(alertController: self.alertMake("Las credenciales enviadas no son correctas"))
                    }
                }
                    
                else{
                    self.animationBtn.stopAnimation()
                    self.display(alertController: self.alertMake("Las credenciales enviadas no son correctas"))
                }
            }
        }
}

