//
//  SplashVC.swift
//  AlertaSegura
//
//  Created by Ubuntu on 1/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class SplashVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func gotoRegister(_ sender: Any) {
        normal = false
       self.gotoNavPresent1("TermsConditionVC")
        
    }
    @IBAction func gotoLogin(_ sender: Any) {
        
        if user.isValid{
           self.gotoVC("MainNav")
        }
        else{
           self.gotoNavPresent1("LoginVC")
        }
    }
    @IBAction func gotoTermsVC(_ sender: Any) {
        self.gotoNavPresent1("TermsConditionVC")
    }
}
