//
//  DropViewVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class DropViewVC: BaseVC1{

    @IBOutlet weak var spaceView: UIView!
    @IBOutlet weak var sampleTable: UITableView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var tableViewHeigh: NSLayoutConstraint!
    
    var cellHeight = 50
    let sampleArray =  ["Hilario Ascasubi","Medanos","Pedro Luro","Mayor Buratovich","Juan Couste","Teniente Origone","Colona San Adolfo","Argerich","Laguna Chasico","Balneario La Chiquita","La Masscota","Paraje Colonoia Monte La Plata","Paraje La Mascota","Paraje Montes De Oca","Paraje Nicolas Levalle","Paraje Ombucta","Otro / No vivo en Villarino"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
             sampleTable.roundCorners([.bottomLeft, .bottomRight], radius: 20)
    }
}

extension DropViewVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        cell.sampleMeal.text = sampleArray[indexPath.row]
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.popViewController(animated: false)
        selectedLocation = sampleArray[indexPath.row]
    }
}

class SampleCell: UITableViewCell {
    @IBOutlet weak var sampleMeal: UILabel!
}
