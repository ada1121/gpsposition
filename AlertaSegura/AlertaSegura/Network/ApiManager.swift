//
//  ApiManager.swift
//  PortTrucker
//
//  Created by PSJ on 9/12/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON

// ************************************************************************//
                                // Location Tracking //
// ************************************************************************//

let API = "http://3.222.185.162/api/"

let REGISTER = API + "register"
let UPDATE = API + "update/"
let LOGIN = API + "login"


struct PARAMS {
    static let ID = "id"
    static let NAME = "name"
    static let DOC_TYPE = "doc_type"
    static let BIRTHDAY = "birth_date"
    static let EMAIL = "email"
    static let PHONECODE = "phone_code"
    static let PHONE = "phone"
    static let HOME_STREET = "home_street"
    static let HOMESTREET_NUMBER = "home_street_number"
    static let HOME_INFO_EXTRAS = "home_info_extras"
    static let HOME_CITY = "home_city"
    static let MEDICAL_INFO = "medical_info"
    static let BLOOD_INFO = "blood_info"
    static let EMERGENCY_CONTACT_PHONE = "emergency_contact_phone"
    static let EMERGENCY_CONTACT_NAME = "emergency_contact_name"
    static let EMERGENCY_CONTACT_RELATIONSHIP = "emergency_contact_relationship"
    static let APIKEY = "api_key"
    static let PASSWORD = "password"
    static let CLIENT_ID = "client_id"
    static let TYPE = "type"
    static let LAT = "lat"
    static let LANG = "lang"
    static let PROFIEL_PICTURE = "profile_picture"
    static let STATUS = "status"
    static let DOC_NUMBER = "doc_number"
    static let EMAIL_VERIFIED_AT = "email_verified_at"
    static let CREATED_AT = "created_at"
    static let UPDATED_AT = "updated_at"
    static let DATA = "data"
    static let DNI = "DNI"
}

class ApiManager {
    
    class func login(useremail : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL : useremail, PARAMS.PASSWORD : password ] as [String : Any]
        
        Alamofire.request(LOGIN, method:.post, parameters:params)
            .responseJSON { response in
            
                print("response??????", response)
                
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    
                    
                    let dict = JSON(data)
                    
                    let user_info = dict[PARAMS.DATA].object
                    let userdata = JSON(user_info)
                    user.clearUserInfo()
                    user = UserModel(userdata)
                    user.saveUserInfo()
                    
                    if user.isValid{
                        completion(true, userdata)
                        print("userdata????? === ",userdata)
                    }
                    else{
                        completion(false, "userdata")
                    }
            }
        }
    }
    
    class func alert(client_id : Int, type : Int,lat : Double, lang : Double, api_key : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = [PARAMS.CLIENT_ID : client_id, PARAMS.TYPE : type , PARAMS.LAT : lat , PARAMS.LANG : lang, PARAMS.APIKEY : api_key ] as [String : Any]
        let requestURL = API + "alert?client_id=" + "\(client_id)" + "&type=" + "\(type)" + "&lat=" + "\(lat)" + "&long=" + "\(lang)" + "&api_key=" + "\(api_key)"
        Alamofire.request(requestURL, method:.post, parameters:params)
            .responseJSON { response in
            
                print("response", response)
                
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    
                    let message = dict["message"].stringValue
                    
                    completion(true, message)
                    print("message????? === ",message)
                    
            }
        }
    }
    
    class func register( name : String,doc_type : String,doc_number : String,birth_date : String,email : String,password : String,phone_code : String,phone : String,home_street : String,home_street_number : String,home_info_extras : String,home_city : String,medical_info : String,blood_info : String,emergency_contact_phone : String,emergency_contact_name : String,emergency_contact_relationship : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.NAME : name, PARAMS.DOC_TYPE : doc_type , PARAMS.DOC_NUMBER : doc_number , PARAMS.BIRTHDAY : birth_date, PARAMS.EMAIL : email, PARAMS.PASSWORD : password , PARAMS.PHONECODE : phone_code , PARAMS.PHONE : phone, PARAMS.HOME_STREET : home_street , PARAMS.HOMESTREET_NUMBER : home_street_number , PARAMS.HOME_INFO_EXTRAS : home_info_extras , PARAMS.HOME_CITY : home_city, PARAMS.MEDICAL_INFO : medical_info, PARAMS.BLOOD_INFO : blood_info , PARAMS.EMERGENCY_CONTACT_PHONE : emergency_contact_phone , PARAMS.EMERGENCY_CONTACT_NAME : emergency_contact_name, PARAMS.EMERGENCY_CONTACT_RELATIONSHIP : emergency_contact_relationship] as [String : Any]
 
        Alamofire.request(REGISTER, method:.post, parameters:params)
            .responseJSON { response in
            
                print("register response===", response)
                
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let user_info = dict[PARAMS.DATA].object
                    let userdata = JSON(user_info)
                    
                    user?.clearUserInfo()
                    user = UserModel(userdata)
                    user?.saveUserInfo()
                    completion(true, userdata)
                    print("userdata === ",userdata)
                    
            }
        }
    }
    
    class func update( name : String,doc_type : String,doc_number : String,birth_date : String,email : String,password : String,phone_code : String,phone : String,home_street : String,home_street_number : String,home_info_extras : String,home_city : String,medical_info : String,blood_info : String,emergency_contact_phone : String,emergency_contact_name : String,emergency_contact_relationship : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let apikey : String = UserDefault.getString(key: PARAMS.APIKEY)!
        let url = UPDATE + "\(user.id ?? 0)"
        let params = [PARAMS.NAME : name, PARAMS.DOC_TYPE : doc_type , PARAMS.DOC_NUMBER : doc_number , PARAMS.BIRTHDAY : birth_date, PARAMS.EMAIL : email, PARAMS.PASSWORD : password , PARAMS.PHONECODE : phone_code , PARAMS.PHONE : phone, PARAMS.HOME_STREET : home_street , PARAMS.HOMESTREET_NUMBER : home_street_number , PARAMS.HOME_INFO_EXTRAS : home_info_extras , PARAMS.HOME_CITY : home_city, PARAMS.MEDICAL_INFO : medical_info, PARAMS.BLOOD_INFO : blood_info , PARAMS.EMERGENCY_CONTACT_PHONE : emergency_contact_phone , PARAMS.EMERGENCY_CONTACT_NAME : emergency_contact_name, PARAMS.EMERGENCY_CONTACT_RELATIONSHIP : emergency_contact_relationship,PARAMS.APIKEY: apikey] as [String : Any]
   
            Alamofire.request(url, method:.post, parameters:params)
                .responseJSON { response in
                
                    print("update response===", response)
                    
                switch response.result {
                    case .failure:
                        completion(false, nil)
                    case .success(let data):
                        let dict = JSON(data)
                        let user_info = dict[PARAMS.DATA].object
                        let userdata = JSON(user_info)
                        
                        user?.clearUserInfo()
                        user = UserModel(userdata)
                        user?.saveUserInfo()
                        
                        completion(true, userdata)
                        print("userdata === ",userdata)
                        
                }
            }
        }
}
